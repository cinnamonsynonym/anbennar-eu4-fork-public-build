estate_daulophs =
{
	icon = 16

	# If true, country will get estate
	trigger = {
		has_reform = teplinbasiet_arena_kingdom_reform
	}

	country_modifier_happy = {
		mercenary_cost = -0.2
		merc_maintenance_modifier = -0.2
		monthly_support_heir_gain = 0.33
	}
	country_modifier_neutral = {
		merc_maintenance_modifier = -0.1
	}	
	country_modifier_angry = {
		merc_maintenance_modifier = 0.2
		global_unrest = 2
	}
	land_ownership_modifier = {
		daulophs_loyalty_modifier = 0.2
	}

	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}

	# Influence modifiers
	base_influence = 10
	influence_modifier = {
		desc = EST_VAMPIRIC_LORD
		trigger = {
			has_estate_privilege = estate_vampires_organization_vampires_lord
		}	
		influence = -10
	}
	influence_modifier = {
		desc = EST_VAL_EASTERN_TECH
		trigger = {
			technology_group = eastern
		}
		influence = 5
	}
	
	# Influence modifiers from Events:
	#influence_modifier = {
	#	desc = EST_HUN_MAGNATES
	#	trigger = {
	#		has_ruler_modifier = hun_power_to_magnates
	#	}
	#	influence = 10
	#}
	influence_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER
		trigger = {
			OR = {
				has_disaster = estate_burghers_disaster
				has_disaster = estate_church_disaster
				has_disaster = estate_mages_disaster
				has_disaster = estate_artificers_disaster
				has_disaster = estate_adventurers_disaster
				#has_disaster = estate_brahmins_disaster
				#has_disaster = estate_jains_disaster
			}
		}	
		influence = -40
	}
	
	loyalty_modifier = {
		desc = EST_VAL_OTHER_ESTATE_IN_POWER_LOY
		trigger = {
			OR = {
				has_disaster = estate_burghers_disaster
				has_disaster = estate_church_disaster
				has_disaster = estate_mages_disaster
				has_disaster = estate_artificers_disaster
				has_disaster = estate_adventurers_disaster
				#has_disaster = estate_brahmins_disaster
				#has_disaster = estate_jains_disaster
			}
		}
		loyalty = -20
	}
#	loyalty_modifier = {
#		desc = EST_VAL_NOBLE_CONSORT
#		trigger = {
#			has_dlc = "Rights of Man"
#			has_consort = yes
#			is_origin_of_consort = ROOT
#		}
#		loyalty = 5
#	}
#	
#	 Great Peasants' War Modifiers
#	
#	influence_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_WIN_MINOR_INFLUENCE
#		trigger = {
#			has_global_flag = gpw_nobles_win_minor
#			is_part_of_hre = yes
#		}
#		influence = 5
#	}
#	
#	loyalty_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_WIN_MINOR_LOYALTY
#		trigger = {
#			has_global_flag = gpw_nobles_win_minor
#			is_part_of_hre = yes
#		}
#		loyalty = 5
#	}
#	
#	influence_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_WIN_MAJOR_INFLUENCE
#		trigger = {
#			has_global_flag = gpw_nobles_win_major
#			is_part_of_hre = yes
#		}
#		influence = 10
#	}
#	
#	loyalty_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_WIN_MAJOR_LOYALTY
#		trigger = {
#			has_global_flag = gpw_nobles_win_major
#			is_part_of_hre = yes
#		}
#		loyalty = 10
#	}
#	
#	influence_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_LOSE_MINOR_INFLUENCE
#		trigger = {
#			has_global_flag = gpw_peasants_win_minor
#			is_part_of_hre = yes
#		}
#		influence = -5
#	}
#	
#	loyalty_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_LOSE_MINOR_LOYALTY
#		trigger = {
#			has_global_flag = gpw_peasants_win_minor
#			is_part_of_hre = yes
#		}
#		loyalty = -5
#	}
#	
#	influence_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_LOSE_MAJOR_INFLUENCE
#		trigger = {
#			has_global_flag = gpw_peasants_win_major
#			is_part_of_hre = yes
#		}
#		influence = -10
#	}
#	
#	loyalty_modifier = {
#		desc = EST_VAL_GREAT_PEASANTS_WAR_NOBLES_LOSE_MAJOR_LOYALTY
#		trigger = {
#			has_global_flag = gpw_peasants_win_major
#			is_part_of_hre = yes
#		}
#		loyalty = -10
#	}
	
	#Anbennar
	
	#Alternative naming of estates
	# custom_name = {
		# desc = estate_beys
		# trigger = {
			# technology_group = ottoman
		# }
	# }
	# custom_name = {
		# desc = estate_boyars
		# trigger = {
			# OR = {
				# culture_group = east_slavic
				# primary_culture = bulgarian
				# primary_culture = romanian
			# }
		# }
	# }
	# custom_name = {
		# desc = estate_szlachta
		# trigger = {
			# OR = {
				# primary_culture = polish
				# primary_culture = lithuanian
			# }
		# }
	# }
	# custom_name = {
		# desc = estate_timawa
		# trigger = {
			# tag = MAS
		# }
	# }
	# custom_name = {
		# desc = estate_nayaks
		# trigger = {
			# tag = VIJ
		# }
	# }
	# custom_name = {
		# desc = estate_samantas
		# trigger = {
			# tag = ORI
		# }
	# }
	# custom_name = {
		# desc = estate_kshatriyas
		# trigger = {
			# religion = hinduism
		# }
	# }
	# custom_name = {
		# desc = estate_qinwang
		# trigger = {
			# has_reform = celestial_empire
		# }
	# }
	# custom_name = {
		# desc = estate_mansabdars
		# trigger = {
			# tag = MUG
		# }
	# }
	# custom_name = {
		# desc = estate_tetecuhtin
		# trigger = {
			# religion = nahuatl
		# }
	# }
	# custom_name = {
		# desc = estate_andriana
		# trigger = {
			# tag = MIR
		# }
	# }
	# custom_name = {
		# desc = estate_amirs
		# trigger = {
			# religion_group = muslim
			# NOT = { technology_group = ottoman } #Should possibly be more restricted
		# }
	# }
	# custom_name = {
		# desc = estate_planters
		# trigger = {
			# tag = USA
		# }
	# }
	# custom_name = {
		# desc = estate_bushi
		# trigger = {
			# culture_group = japanese_g
		# }
	# }
	# custom_name = {
		# desc = estate_yangban_administrators
		# trigger = {
			# tag = KOR
			# religion = confucianism
		# }
	# }
	# custom_name = {
		# desc = estate_junkers
		# trigger = {
			# OR = {
				# has_reform = prussian_republic_reform
				# has_reform = prussian_monarchy
				# tag = GER
			# }
		# }
	# }
#	custom_name = {
#		desc = estate_companions
#		trigger = {
#			tag = G32
#		}
#	}

	color = { 200 0 50 }
	
	privileges = {
		estate_daulophs_land_rights #in
		estate_daulophs_grand_mercenary_feast #in
		estate_daulophs_arena_seats #in
		estate_daulophs_daily_matches #in
		estate_daulophs_weekly_matches #in
		estate_daulophs_monthly_matches #in
		estate_daulophs_dauloph_primacy #in
		estate_daulophs_police_subjects #in
		estate_daulophs_monopoly_of_metals #in
		estate_daulophs_monopoly_of_livestock #in
		estate_daulophs_statutory_rights #in
	}

	agendas = {
		estate_daulophs_get_allies #in
		estate_daulophs_befriend_rival_of_rival #in
		estate_daulophs_relations_with_X #in
		estate_daulophs_break_coalition #in
		estate_daulophs_condottieri_rivals #in
		estate_daulophs_support_independence #in
		estate_daulophs_retake_core #in
		estate_daulophs_crush_revolts #in
		estate_daulophs_regain_liberty #in
		estate_daulophs_protect_our_culture #in
		estate_daulophs_vassalise_vulnerable_country #in
		estate_daulophs_expand_into_x #in
		estate_daulophs_complete_conquest_of_x #in
		estate_daulophs_build_an_army #in
		estate_daulophs_bigger_army_than_rival #in
		estate_daulophs_recover_abysmal_prestige #in
		estate_daulophs_improve_prestige #in
		estate_daulophs_hire_advisor #in
		estate_daulophs_fire_advisor #in
	}
	
	influence_from_dev_modifier = 1.0
}