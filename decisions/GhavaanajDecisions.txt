

country_decisions = {
	ghavaanaj_elephant_herd_screen = {
		major = yes
		potential = {
			has_country_flag = ghavaanaj_elephant_herd_mechanic
		}
		
		allow = {
			hidden_trigger = {
				NOT = { has_country_flag = ghavaanaj_herd_menu_open }
			}
		}
	
		effect = {
			country_event = { id = ghavaanaj.2000 }
			hidden_effect = {
				save_global_event_target_as = ghavaanaj_herd_country #This is needed to update after tag change, otherwise training doesn't work
			}
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				always = yes
			} 
		}
	}
	
	ghavaanaj_herd_secrets = {
		potential = {
			primary_culture = ghavaanaj
		}
		
		allow = {
			hidden_trigger = {
				NOT = { has_country_flag = ghavaanaj_herd_unlock_menu_open }
			}
		}
	
		effect = {
			country_event = { id = ghavaanaj.1000 }
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				always = yes
			} 
		}
	}
	
	ghavaanaj_expeditions = {
		potential = {
			has_country_flag = ghavaanaj_expeditions_mechanic
		}
		
		allow = {
			always = no
		}
	
		effect = {
			#Event: Expeditions Menu
		}
		
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				always = yes
			} 
		}
	}
}



